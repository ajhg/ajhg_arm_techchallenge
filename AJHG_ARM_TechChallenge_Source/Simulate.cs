﻿/*
 * The Author believes that nearly all of the code in this source code file is structured in a way that should be easy to read. 
 * Sections where the author believes that this is not the case have been marked as such.
 */

/// <para>`using` statements import System libraries.</para>
using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
///     <para>Project Namespace - AJHG_ARM_TechChallenge</para>
/// </summary>
namespace AJHG_ARM_TechChallenge
{
    /// <summary>
    ///     <para>Simulate class - For running the main simulation.</para>
    /// </summary>
    class Simulate
    {
        /// <para>Variable of type IList to hold the data of the conveyor belt.</para>
        public static IList<int> conveyorBelt = new List<int>();

        /// <summary>
        ///     <para>
        ///         Main function is a public static function. It starts by handling the arguments that are passed into the software. It then generates a random conveyor belt using <see cref="System.Random"/>. Then, for every slot on the conveyor belt, for every iteration of the simulation, the program does the following; outputting debug data when required. It checks to see if either worker can assemble a product and if that worker can, it then does so. Or, if the worker has a fully assembled product then it places it on the conveyor belt if there is space for it to do so. Alternativly, if the worker is missing a component to assemble a product then it checks to see if the component on the conveyor belt is a required component and picks it up if it is. Finally, the function increments the producton of products.
        ///     </para>
        /// </summary>
        /// <param name="args">
        ///     The arguments used by the software to decide how long the conveyor belt should be and the number of times that the simulation should run as well as whether or not the software should output debug data and how that should be displayed.
        /// </param>
        /// <seealso cref="System.Random"/>
        public static void Main(string[] args)
        {
            if (args.Length <= 1)
            {
                throw new Exception("The simulation run count and the conveyor slots cannot be 0 or null.");
            }
            int.TryParse(args[0], out int simulationRunCount);
            int.TryParse(args[1], out int conveyorSlots);
            bool debug = false;
            bool chars = false;
            for (int i = 0; i <= args.Length; i++)
            {
                if (args.Length == 3)
                {
                    /// <para>Code block sets the value of variable `debug` to true if the `--debug` switch is present otherwise it sets it to false.</para>
                    debug = args[2] == "--debug" || args[2] == "-d" ? true : false;
                }
                else if (args.Length == 4)
                {
                    /// <para>Code block sets the value of variable `debug` to true if the `--debug` switch is present otherwise it sets it to false.</para>
                    debug = args[2] == "--debug" || args[2] == "-d" ? true : false;
                    /// <para>Code block sets the value of variable `chars` to true if the `--chars` switch is present otherwise it sets it to false.</para>
                    chars = args[3] == "--chars" || args[3] == "-c" ? true : false;
                }
            }
            Random random = new Random();
            var printString = "";
            for (int i = 0; i <= simulationRunCount; i++)
            {
                var newConveyorBeltItem = random.Next(5);
                if (newConveyorBeltItem == 1 || newConveyorBeltItem == 4) conveyorBelt.Add(1);
                else conveyorBelt.Add(newConveyorBeltItem);
                if (chars)
                {
                    printString += TypeCodeToString(conveyorBelt[i]) + ", "; 
                }
                else
                {
                    printString += conveyorBelt[i].ToString() + ", ";
                }
                
            }
            if (debug)
            {
                Console.WriteLine(printString);
            }
            for (int i = 0; i <= conveyorSlots; i++)
            {
                if (debug)
                {
                    Console.WriteLine("");
                    Console.WriteLine("---");
                    Console.WriteLine("");
                }
                WorkerPair workerPair = new WorkerPair();
                int leftProducingCount = -1;
                int rightProducingCount = -1;
                bool leftPerformedOperation = false;
                bool rightPerformedOperation = false;
                printString = "";
                for (int j = 0; j <= simulationRunCount; j++)
                {
                    leftPerformedOperation = false;
                    rightPerformedOperation = false;
                    if (debug)
                    {
                        Console.WriteLine("");
                        Console.WriteLine("Current Simulation Run Count: " + j.ToString());
                        Console.WriteLine("---");
                        Console.WriteLine("Pre Operation Data:");
                        OutputData(chars, workerPair);
                        if (chars)
                        {
                            Console.WriteLine(" | Conveyor Belt: " + TypeCodeToString(conveyorBelt[j]));
                        }
                        else
                        {
                            Console.WriteLine(" | Conveyor Belt: " + conveyorBelt[j].ToString());
                        }
                    }
                    if (workerPair.leftWorker[0] == 1 && !leftPerformedOperation)
                    {
                        if (workerPair.leftWorker[1] == 2)
                        {
                            leftProducingCount = 3;
                            workerPair.leftWorker = new int[] { 0, 4 };
                            leftPerformedOperation = true;
                        }
                        else if (workerPair.leftWorker[1] == 3)
                        {
                            leftProducingCount = 3;
                            workerPair.leftWorker = new int[] { 0, 5 };
                            leftPerformedOperation = true;
                        }
                    }
                    if (workerPair.rightWorker[0] == 1 && !rightPerformedOperation)
                    {
                        if (workerPair.rightWorker[1] == 2)
                        {
                            rightProducingCount = 3;
                            workerPair.rightWorker = new int[] { 0, 4 };
                            rightPerformedOperation = true;
                        }
                        else if (workerPair.rightWorker[1] == 3)
                        {
                            rightProducingCount = 3;
                            workerPair.rightWorker = new int[] { 0, 5 };
                            rightPerformedOperation = true;
                        }
                    }
                    if (leftProducingCount < 0 && (workerPair.leftWorker[0] == 0 && (workerPair.leftWorker[1] == 4 || workerPair.leftWorker[1] == 5)) && conveyorBelt[j] == 0 && !leftPerformedOperation)
                    {
                        if (workerPair.leftWorker[1] == 4)
                        {
                            conveyorBelt[j] = 4;
                        }
                        else if (workerPair.leftWorker[1] == 5)
                        {
                            conveyorBelt[j] = 5;
                        }
                        workerPair.leftWorker[1] = 0;
                        leftPerformedOperation = true;
                    }
                    else if (rightProducingCount < 0 && (workerPair.rightWorker[0] == 0 && (workerPair.rightWorker[1] == 4 || workerPair.rightWorker[1] == 5)) && conveyorBelt[j] == 0 && !rightPerformedOperation)
                    {
                        if (workerPair.rightWorker[1] == 4)
                        {
                            conveyorBelt[j] = 4;
                        }
                        else if (workerPair.rightWorker[1] == 5)
                        {
                            conveyorBelt[j] = 5;
                        }
                        workerPair.leftWorker[1] = 0;
                        rightPerformedOperation = true;
                    }
                    if (conveyorBelt[j] == 1)
                    {
                        if (!((workerPair.leftWorker[0] == 0 && workerPair.leftWorker[1] == 4) || (workerPair.leftWorker[0] == 0 && workerPair.leftWorker[1] == 5)) && leftProducingCount < 0 && !leftPerformedOperation)
                        {
                            workerPair.leftWorker[0] = 1;
                            conveyorBelt[j] = 0;
                            leftPerformedOperation = true;
                        }
                        else if (!((workerPair.rightWorker[0] == 0 && workerPair.rightWorker[1] == 4) || (workerPair.rightWorker[0] == 0 && workerPair.rightWorker[1] == 5)) && rightProducingCount < 0 && !rightPerformedOperation)
                        {
                            workerPair.rightWorker[0] = 1;
                            conveyorBelt[j] = 0;
                            rightPerformedOperation = true;
                        }
                    }
                    else if (conveyorBelt[j] == 2)
                    {
                        if (workerPair.leftWorker[1] == 0 && leftProducingCount < 0 && !leftPerformedOperation)
                        {
                            workerPair.leftWorker[1] = 2;
                            conveyorBelt[j] = 0;
                            leftPerformedOperation = true;
                        }
                        else if (workerPair.rightWorker[1] == 0 && rightProducingCount < 0 && !rightPerformedOperation)
                        {
                            workerPair.rightWorker[1] = 2;
                            conveyorBelt[j] = 0;
                            rightPerformedOperation = true;
                        }
                    }
                    else if (conveyorBelt[j] == 3)
                    {
                        if (workerPair.leftWorker[1] == 0 && leftProducingCount < 0 && !leftPerformedOperation)
                        {
                            workerPair.leftWorker[1] = 3;
                            conveyorBelt[j] = 0;
                            leftPerformedOperation = true;
                        }
                        else if (workerPair.rightWorker[1] == 0 && rightProducingCount < 0 && !rightPerformedOperation)
                        {
                            workerPair.rightWorker[1] = 3;
                            conveyorBelt[j] = 0;
                            rightPerformedOperation = true;
                        }
                    }
                    leftProducingCount--;
                    rightProducingCount--;
                    if (debug)
                    {
                        Console.WriteLine("Post Operation Data:");
                        OutputData(chars, workerPair);
                        if (chars)
                        {
                            Console.WriteLine(" | Conveyor Belt: " + TypeCodeToString(conveyorBelt[j]));
                        }
                        else
                        {
                            Console.WriteLine(" | Conveyor Belt: " + conveyorBelt[j].ToString());
                        }
                    }
                }
                if (debug)
                {
                    Console.WriteLine("");
                    printString = "";   
                    for (int j = 0; j <= simulationRunCount; j++)
                    {
                        if (chars)
                        {
                            printString += TypeCodeToString(conveyorBelt[i]) + ", ";
                        }
                        else
                        {
                            printString += conveyorBelt[i].ToString() + ", ";
                        }
                    }
                    Console.WriteLine("");
                    Console.WriteLine(printString);
                }
            }
            Console.WriteLine("");
            int totalWasted = 0;
            int totalProduced = 0;
            for (int i = 0; i < conveyorBelt.Count; i++)
            {
                if (conveyorBelt[i] == 1 || conveyorBelt[i] == 2 || conveyorBelt[i] == 3)
                {
                    totalWasted++;
                }
                else if (conveyorBelt[i] == 4 || conveyorBelt[i] == 5)
                {
                    totalProduced++;
                }
            }
            Console.WriteLine("Total Wasted: " + totalWasted.ToString());
            Console.WriteLine("Total Produced: " + totalProduced.ToString());
        }

        /// <summary>
        ///     <para>
        ///         Function outputs data about the current state of the factory based on the workerPair passed in.
        ///     </para>
        /// </summary>
        /// <param name="chars">Tells the function wheather or not it should use characters instead of intagers. True if it should.</param>
        /// <param name="workerPair">The worker pair to get details from.</param>
        public static void OutputData(bool chars, WorkerPair workerPair)
        {
            if (chars)
            {
                Console.WriteLine(" | Left Worker: " + TypeCodeToString(workerPair.leftWorker[0]) + "," + TypeCodeToString(workerPair.leftWorker[1]));
                Console.WriteLine(" | Right Worker: " + TypeCodeToString(workerPair.rightWorker[0]) + "," + TypeCodeToString(workerPair.rightWorker[1]));
            }
            else
            {
                Console.WriteLine(" | Left Worker: " + workerPair.leftWorker[0].ToString() + "," + workerPair.leftWorker[1].ToString());
                Console.WriteLine(" | Right Worker: " + workerPair.rightWorker[0].ToString() + "," + workerPair.rightWorker[1].ToString());
            }
        }

        /// <summary>
        ///     <para>Converts integers into characters if the software is run with the `--chars` switch.</para>
        /// </summary>
        /// <param name="typeCode">The integer to convert to a character.</param>
        /// <returns>The typecode as a character, based on the integer passed in.</returns>
        public static string TypeCodeToString(int typeCode) => typeCode switch
        {
            1 => "A",
            2 => "B",
            3 => "C",
            4 => "P",
            5 => "Q",
            _ => "Empty",
        };
    }
}
