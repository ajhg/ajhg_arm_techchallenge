﻿/*
 * The Author believes that nearly all of the code in this source code file is structured in a way that should be easy to read. 
 * Sections where the author believes that this is not the case have been marked as such.
 */

/// <para>`using` statements import System libraries.</para>
using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
///     <para>Project Namespace - AJHG_ARM_TechChallenge</para>
/// </summary>
namespace AJHG_ARM_TechChallenge
{
    /// <summary>
    ///     <para>WorkerPair Class. Stores the data of each worker pair in an easy to handle format.</para>
    /// </summary>
    class WorkerPair
    {
        /// <para>Arrays to store the WorkerPair data.</para>
        public IList<int> leftWorker = new List<int>();
        public IList<int> rightWorker = new List<int>();

        /// <summary>
        ///     <para>Constuctor creates a new instance of the WorkerPair class. It adds two elements to each worker array with a value of 0.</para>
        /// </summary>
        public WorkerPair() 
        {
            leftWorker.Add(0);
            leftWorker.Add(0);
            rightWorker.Add(0);
            rightWorker.Add(0);
        }
    }
}
