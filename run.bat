@echo off
echo Plese enter the following details about the simualtion you want to run.
echo ---
echo The first two variables must be ints but the last two can be left blank.
echo They are for debugging and output perposes only.
set /p simulationRunCount="Simulation run count: "
set /p conveyorSlots="Number of conveyor belt slots: "
echo The following switches can be left blank.
set /p debug="Debug? (Can be left Blank): "
set /p chars="As characters? (Can be left Blank): "
AJHG_ARM_TechChallenge_0.1.2-0\AJHG_ARM_TechChallenge %simulationRunCount% %conveyorSlots% %debug% %chars%
set /p
PAUSE