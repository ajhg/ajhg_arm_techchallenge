# AJHG ARM Tech Challenge

## Assumptions

 - It is assumed that it is perfectly fine for workers to only use one hand for one type of part.
 - It is assumed that details of each step do not need to be desplayed to the user but I have taken the liberty of providing functionality to do this should the user desire.
 - It is assumed that C#.NET is a suitable language to produce the software in.
 - It is assumed that a conveyor belt slot can hold nothing. 
 - It is assumed that a worker can hold nothing in either hand.
 - It is assumed that the conveyor belt moves forward by 1 every round of the simulation.
 - It is assumed that components can be picked up in any order.

## Compiling the project in Visual Studio 2019

Dependencies:

 | .NET Core 3.1 (I used the version included with Visual Studio 2019.) 

     | *[Standalone .NET Core Downloads and Source](https://dotnet.microsoft.com/)*

     | *[Visual Studio 2019 Download](https://visualstudio.microsoft.com/downloads/)*

1. Load `AJHG_ARM_TechChallenge_Source\AJHG_ARM_TechChallenge.sln` in Visual Studio. (I wrote this in Visual Studio 2019.)
2. Under the `Project` menu strip item, click `AJHG_ARM_TechChallenge Properties...`.
3. Under the `Debug` tab within the poperties file enter the application arguments following the argument and switch guidance below.
4. Under the `Build` menu strip item, click `Build Solution` (Ctrl+Shift+B) and wait for the build to finish.
5. Under the `Debug` menu strip item, click `Start Debugging` (F5) and a command line will pop up with the output from the software.

## Running the software in the command line. 

1. Navigate to the `AJHG_ARM_TechChallenge_0.1.2-0` in the command line.
2. Execute the `AJHG_ARM_TechChallenge.exe` file with your chosen command line arguments and switches following the below guidance.

## Running the software like an application. 

Run the `run.bat` file in the top directory and follow the instructions in the bat file.

## Command Line Arguments And Switches

Arguments 1 and 2 are required for the software to run or else it will throw an exception.
    
Arguments 3 amd 4 are optional. The software will still run if these switches are not provided. 

__**Please ensure that you provide the arguments in the order listed below.**__

Argument 1: The number of times you want the simulation to run for. Formatted as a whole intager.

Argument 2: The number of conveyor belt slots (and therefore worker pairs) to simulate. Formatted as an whole intager.

Argument 3 (Optional): `--debug` or `-d`. Tells the software whether or not it should output debug data or not. Outputs debug data as intagers by default unless specified as characters using the `--chars` switch.

Argument 4 (Optional): `--chars` or `-c`. Tells the software to output the debug data as intagers or characters. `--debug` outputs debug data as intagers if this switch is not present.

---

If you have any issues please feel free to email me: [ajhg@tutanota.com](mailto:ajhg@tutanota.com)

This software is licenced under a GNU AGPL 3.0 International License. The full license can be obtained from the `LICENSE` file or at the link below.

[https://www.gnu.org/licenses/agpl-3.0.en.html](https://www.gnu.org/licenses/agpl-3.0.en.html)